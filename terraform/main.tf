terraform {
  required_version = "= 1.4.2"
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  token     = ""
  cloud_id  = ""
  folder_id = ""
  zone      = "ru-central1-a"
  
}

resource "yandex_compute_instance" "default" {
  name        = "pc-terraform"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd8emvfmfoaordspe1jr"
      size     = 15
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.foo.id}"
    nat       = true
  }

  metadata = {
    foo      = "bar"
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_vpc_network" "foo" {
    name        = "network-1"
}

resource "yandex_vpc_subnet" "foo" {
    name           = "subnet-1"
    v4_cidr_blocks = ["10.2.0.0/16"]
    zone           = "ru-central1-a"
    network_id     = "${yandex_vpc_network.foo.id}"
}

output "instance_external_ip" {
  value = yandex_compute_instance.default.network_interface.0.nat_ip_address
}
